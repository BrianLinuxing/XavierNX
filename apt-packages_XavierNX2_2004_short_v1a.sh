set -o errexit
set -v
#!/bin/sh
#
#Licence: MIT License
#
#Copyright All Rights Reserved, 2022, Brian Byrne. 
#
#Purpose:
# 1. Set up a standard Ubuntu/Debian based system with the required apps.
# 2. It is a quick and dirty way of updating packages, post the initial installation.
# 3. Various incarnations since about 2008, or before.
# 4. This is a smaller selection than previously due to space limitations and slower processor speeds.
#
#Author(s): B.Byrne.
#
#Original date: May 2020
#
#Nvidia Jetpack 4.6 (April 2023). Initial kernel of 4.9.253-tegra, repo:  Ubuntu 20.04
#
#Notes: 
# 1. It's now distro specific,i.e. Debian vs. Ubuntu, to avoid minor repo errors.
# 2. Currently, it is based on my Diet Pi example, parts from my 2006 iMac LXLE setup too (May 2020).
# 3. It makes no assumptions about pre-existing packages that might already have been installed.
# 4. Some apps are missing from the repo, eg. zenmap, etc and will needed to be inserted later on.
# 5. Mostly grouped alphabetically, or where they are linked directly package-wise, eg. zenmap
# 6. Remember to make it executable.
# 7. NB: Use at own risk, your mileage will vary, it works for me, but maybe not others :)
# 8. Best to execute with tee -a via sudo with a time command too.
# eg. time sudo ./apt-packages_XavierNX2_2004_short_v1a.sh| tee -a NX_additional_apps_$(date '+%Y-%m-%d_%H%M%S')_$(hostname).txt
#
#Exceptions for Nano Pi devices or those without any video connector.
#
# 1. Install a minimal X first like this:
#
#apt-get update
#
#apt-get install -y -V xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core xinit x11-xserver-utils xauth xterm x11-apps x11-utils xfonts-base xfonts-100dpi xfonts-75dpi
#
#-----------------------------------------------------------------------------
#
#TDB: exit code, more debugging options and timing functions.
#
#-----------------------------------------------------------------------------
#
apt-get update
#
apt-get -y -V install apt-transport-https apt-utils apt-listchanges
#
apt-get -y -V install curl
#
apt-get -y -V install deluge deluge-common deluge-gtk
#
apt-get -y -V install ddrescueview gddrescue
#
apt-get -y -V install debian-goodies lsb-release
#
apt-get -y -V install fakeroot
#
apt-get -y -V install firefox firefox-locale-en gthumb
#
apt-get -y -V install git-gui gitk gitweb
#
apt-get -y -V install gparted ntfs-3g mtools kpartx
#
apt-get -y -V install gdebi gnome-terminal grsync 
#
apt-get -y -V install hardinfo hunspell-en-gb idle3 inxi iotop 
#
apt-get -y -V install k4dirstat 
#
apt-get -y -V install leafpad
#
apt-get -y -V install libreoffice-l10n-en-gb libreoffice mythes-en-us default-jre libreoffice-java-common
#
apt-get -y -V install lshw lsof lxterminal
#
apt-get -y -V install make meld moreutils mtr nano net-tools ncdu nmon
#
apt-get -y -V install needrestart
#
apt-get -y -V install okular pluma powertop rsync
#
apt-get -y -V install pcmanfm libfm-tools
#
apt-get -y -V install screen spyder spyder3
#
apt-get -y -V install tree trash-cli 
#
apt-get -y -V install xarchiver unzip arj lhasa lrzip lzip lzop ncompress zstd zip bzip2 bzr p7zip-full xz-utils
#
apt-get -y -V install vlc wget whois x11-apps
#
#-----------------------------------------------------------------------------
# Build tools
#
apt-get -y -V install python3 gettext
#
apt-get -y -V install python3-pip
#
apt-get -y -V install python3-spyder python3-openssl flake8 python3-pycurl
#
apt-get -y -V install python3-brotli libbrotli-dev libbz2-dev
#
apt-get -y -V install libffi-dev libreadline-dev
#
apt-get -y -V install uncrustify
#
apt-get -y -V install pkg-config
#
apt-get -y -V install cmake build-essential gcc-arm-none-eabi libnewlib-arm-none-eabi
#
#-----------------------------------------------------------------------------
exit